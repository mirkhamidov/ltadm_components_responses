<?
$items=array(
  "unixtime" => array(
         "desc" => "Дата публикации",
         "type" => "date",
         "fields" => array(
                      "d" => array("name" => "День","size" => "2", "after" => "."),
                      "m" => array("name" => "Месяц","size" => "2", "after" => "."),
                      "Y" => array("name" => "Год","size" => "4"),
                      "H" => array("name" => "Часы","size" => "2", "after" => ":"),
                      "i" => array("name" => "Минуты","size" => "2"),
                      ),
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),

  "author" => array(
         "desc" => "Автор",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
       ),


  "content" => array(
         "desc" => "Текст отзыва",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),
);
?>